﻿Module Module1

    Sub Main()
        Dim pc1 As New ProductoClase1
        pc1.Precio = 100
        Dim pc2 As New ProductoClase2
        pc2.Precio = 100

        Dim iva As New IVA

        Console.WriteLine(String.Format("El total para Producto Clase 1 es: {0}", pc1.Aceptar(iva)))
        Console.WriteLine(String.Format("El total para Producto Clase 2 es: {0}", pc2.Aceptar(iva)))


        Console.ReadKey()
    End Sub

End Module
