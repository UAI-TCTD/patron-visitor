﻿Imports IVA

Public Class IVA
    Implements IVisitor
    Private impuestoClase1 As Double = 1.21
    Private impuestoClase2 As Double = 1.105

    Public Function Visitar(p As ProductoClase2) As Double Implements IVisitor.Visitar
        Return p.Precio * impuestoClase2
    End Function

    Public Function Visitar(p As ProductoClase1) As Double Implements IVisitor.Visitar
        Return p.Precio * impuestoClase1
    End Function
End Class
